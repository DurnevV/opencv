#include "EtalonCorrelation.h"

EtalonCorrelationSearch::EtalonCorrelationSearch(std::string mainImageName, short actionType, float factor){
	this->actionType = actionType;
	this->factor = factor;
	this->mainImage = cv::imread("squareMain.bmp", CV_LOAD_IMAGE_GRAYSCALE);
}

cv::Mat EtalonCorrelationSearch::imageProcessing(cv::Mat& searchImage){
	cv::Mat mainConvert32, searchConvert32;
	cv::Mat mainConvert8, searchConvert8, rezult;
	double meanRefMain = cv::mean(mainImage).val[0];
	double meanRefSearch = cv::mean(searchImage).val[0];

	mainImage.convertTo(mainConvert32, CV_32F, 1.0, -meanRefMain);
	searchImage.convertTo(searchConvert32, CV_32F, 1.0, -meanRefSearch);

	searchConvert8 = normalizeImage(searchConvert32);
	mainConvert8 = normalizeImage(mainConvert32);

	cv::Mat temprezImageMain;

	switch (actionType){
	case 0:
		matchTemplate(mainConvert8, searchConvert8, rezult, CV_TM_CCORR_NORMED | CV_TM_CCORR);
		break;
	case 1:
		cv::resize(mainConvert8, temprezImageMain, cv::Size(0, 0), factor, factor, cv::INTER_LANCZOS4);
		matchTemplate(temprezImageMain, searchConvert8, rezult, CV_TM_CCORR_NORMED | CV_TM_CCORR);
		break;
	case 2:
		rotateImage(mainConvert8, temprezImageMain, factor);
		matchTemplate(temprezImageMain, searchConvert8, rezult, CV_TM_CCORR_NORMED | CV_TM_CCORR);
		break;
	default:
		printf("wrong actionType/n");
		break;
	}
	return rezult;
}

cv::Mat EtalonCorrelationSearch::normalizeImage(cv::Mat& image){
	double maxVal, minVal;
	cv::Mat retConvert8Image;
	cv::minMaxLoc(image, &minVal, &maxVal);

	double k = 255.0 / (maxVal - minVal);
	image.convertTo(retConvert8Image, CV_8U, k, -minVal*k);

	return retConvert8Image;
}

void EtalonCorrelationSearch::rotateImage(cv::Mat& sourceImage, cv::Mat& dstImage, double angle){
	int len = std::max(sourceImage.cols, sourceImage.rows);
	cv::Point2f pt(len / 2., len / 2.);
	cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
	cv::warpAffine(sourceImage, dstImage, r, cv::Size(len, len));
}