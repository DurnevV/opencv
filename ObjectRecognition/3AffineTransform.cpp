#include "AffineTransform.h"

double AffineTransform::xPlus(double x, double y, pair<double, double> centerMass) {
	return (1 / mu) * ((x - centerMass.first) * cos(-th) - (y - centerMass.second) * sin(-th)) *
		cos(th) - ((x - centerMass.first) * sin(-th) + (y - centerMass.second) * cos(-th)) * sin(th);
}

double AffineTransform::yPlus(double x, double y, pair<double, double> centerMass) {
	return (1 / mu) * ((x - centerMass.first) * cos(-th) - (y - centerMass.second) * sin(-th)) *
		sin(th) + ((x - centerMass.first) * sin(-th) + (y - centerMass.second) * cos(-th)) * cos(th);
}

pair<double, double> AffineTransform::centerMassCalc(Mat& image) {
	double centerMassX = 0, centerMassY = 0, T = 0;
	for (int i = 0; i < image.rows; i++){
		for (int j = 0; j < image.cols; j++){
			T += image.at<uchar>(i, j);
			centerMassX += i * image.at<uchar>(i, j);
			centerMassY += j * image.at<uchar>(i, j);
		}
	}
	return pair<double, double>(centerMassX / T, centerMassY / T);
}

Mat AffineTransform::imageProcessing(Mat& image) {
	double B = 0;
	double C = 0;
	double D = 0;
	std::pair<double, double> centerMass = centerMassCalc(image);
	for (int i = 0; i < image.rows; i++) {
		for (int j = 0; j < image.cols; j++) {
			B += image.at<uchar>(i, j) * ((i - centerMass.first) * (i - centerMass.first) - (j - centerMass.second) * (j - centerMass.second));
			C += image.at<uchar>(i, j) * 2 * (i - centerMass.first) * (j - centerMass.second);
			D += image.at<uchar>(i, j) * ((i - centerMass.first) * (i - centerMass.first) + (j - centerMass.second) * (j - centerMass.second));
		}
	}
	th = 0.5 * atan2(C, B);
	mu = sqrt((D + sqrt(B * B + C * C)) / (D - sqrt(B * B + C * C)));

	double M = 0, T = 0;
	double xTemp = 0;
	double yTemp = 0;
	for (int i = 0; i < image.rows; i++){
		for (int j = 0; j < image.cols; j++){
			xTemp = xPlus(i, j, centerMass);
			yTemp = yPlus(i, j, centerMass);
			M += image.at<uchar>(i, j) * sqrt(xTemp * xTemp + yTemp * yTemp);
			T += image.at<uchar>(i, j);
		}
	}
	M = M / (40 * T);

	double Mdevided = 1 / M;

	cout  << th << " " << mu << " " << Mdevided << "\n";
	cout << centerMass.first << " " << centerMass.second;

	centerMass.first = image.rows;
	centerMass.second = image.cols;
	Mat result(image.rows, image.cols, CV_8U);
	for (int i = 0; i < image.rows; i++){
		for (int j = 0; j < image.cols; j++){
			int temp1 = xPlus(i, j, centerMass) * Mdevided;
			int temp2 = yPlus(i, j, centerMass) * Mdevided;
			if (temp1 < 0) {
				temp1 += image.rows;
			}
			if (temp2 < 0) {
				temp2 += image.cols;
			}
			result.at<uchar>(temp1, temp2) = image.at<uchar>(i, j);
		}
	}
	return result;
} 

void AffineTransform::showImage(const Mat* imageToShow) {
	cv::namedWindow("image", CV_WINDOW_AUTOSIZE);
	imshow("image", *imageToShow);
	cv::waitKey();
}
