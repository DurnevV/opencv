#include "OtsuThresholding.h"

OtsuThresholding::OtsuThresholding(bool multiOtsu){
	this->multiOtsu = multiOtsu;
}

cv::Mat OtsuThresholding::imageProcessing(cv::Mat& addImage){
	if (multiOtsu){
		return otsuMultiTheresholding(addImage);
	} else {
		return otsuBinThreshholding(addImage);
	}
	/*unsigned* histogram = new unsigned[256];
	for (size_t i = 0; i < addImage.rows; ++i){
		for (size_t j = 0; j < addImage.cols; ++j){
			histogram[addImage.at<uchar>(i,j)]++;
		}
	}*/
}

cv::Mat OtsuThresholding::otsuMultiTheresholding(cv::Mat& image){
	cv::Mat outputBin;
	cv::Mat  // vLuciv@mail.ru �������� ��� �� �����
		imageCopy1, imageCopy1Otsu,
		imageCopy2, imageCopy2Otsu;

	double maxValueBin;
	cv::minMaxLoc(image, 0, &maxValueBin);
	//cv::GaussianBlur(addImage, outputBinNormilize, cv::Size(3, 3), 0);
	
	double optimalBinThresh = cv::threshold(image, outputBin, 0, maxValueBin, CV_THRESH_BINARY | CV_THRESH_OTSU);
	image.convertTo(imageCopy1, image.type(), 1.0, optimalBinThresh);

	double maxValueTri;
	cv::minMaxLoc(imageCopy1, 0, &maxValueTri);
	double optimalTriThresh = cv::threshold(imageCopy1, imageCopy1Otsu, 0, maxValueBin, CV_THRESH_BINARY | CV_THRESH_OTSU);
	cv::threshold(imageCopy1, imageCopy1Otsu, optimalTriThresh, maxValueBin, CV_THRESH_BINARY | CV_THRESH_OTSU);

	image.convertTo(imageCopy2, image.type(), 1.0, -(255 - optimalBinThresh)); // �� 4 ������ ������ �������� optimalBinThresh �� optimalTriTresh
	cv::minMaxLoc(imageCopy2, 0, &maxValueTri);
	optimalTriThresh = cv::threshold(imageCopy2, outputBin, 0, maxValueTri, CV_THRESH_BINARY | CV_THRESH_OTSU);
	cv::threshold(imageCopy2, outputBin, optimalTriThresh, maxValueTri, CV_THRESH_BINARY | CV_THRESH_OTSU);
	
	outputBin.convertTo(outputBin, CV_32F, 1.0, optimalTriThresh);

	cv::Mat output = cv::Mat(imageCopy1);
	imageCopy2.copyTo(output, imageCopy1);

	return output;
}

cv::Mat OtsuThresholding::otsuBinThreshholding(cv::Mat& image){
	cv::Mat outputBin;
	
	double maxValue;
	cv::minMaxLoc(image, 0, &maxValue);
	
	double optimalThreshValue = cv::threshold(image, outputBin, 0, maxValue, CV_THRESH_BINARY | CV_THRESH_OTSU);
	cv::threshold(image, outputBin, optimalThreshValue, maxValue, CV_THRESH_BINARY | CV_THRESH_OTSU);

	return outputBin;
}