#include "LawsEnergyMap.h"

LawsEnergyMap::LawsEnergyMap() {
	// T
	float L5m[] = { 1, 4, 6, 4, 1 };
	float E5m[] = { -1, -2, 0, 2, 1 };
	float R5m[] = { 1, -4, 6, -4, 1 };
	float S5m[] = { -1, 0, 2, 0, -1 };

	L5 = cv::Mat(5, 1, CV_32F, L5m).clone();
	E5 = cv::Mat(5, 1, CV_32F, E5m).clone();
	R5 = cv::Mat(5, 1, CV_32F, R5m).clone();
	S5 = cv::Mat(5, 1, CV_32F, S5m).clone();
	//std::cout << L5.at<float>(2, 0);
}

LawsEnergyMap::~LawsEnergyMap() {
	L5.release();
	E5.release();
	S5.release();
	R5.release();
	for (size_t i = 1; i < 10; ++i){
		laws[i].release();
	}
}

cv::Mat LawsEnergyMap::imageProcessing(cv::Mat& main) {
	cv::Mat mainImage = normedImage(main);
	std::unordered_map<std::string, cv::Mat> lawsMass;
	lawsMass.reserve(16);
	lawsMass["mL5E5"] = std::move(convolutionWithMask(mainImage, masksGenerator(L5, E5)));
	lawsMass["mE5L5"] = std::move(convolutionWithMask(mainImage, masksGenerator(E5, L5)));
	lawsMass["mL5S5"] = std::move(convolutionWithMask(mainImage, masksGenerator(L5, S5)));
	lawsMass["mS5L5"] = std::move(convolutionWithMask(mainImage, masksGenerator(S5, L5)));
	lawsMass["mL5R5"] = std::move(convolutionWithMask(mainImage, masksGenerator(L5, R5)));
	lawsMass["mR5L5"] = std::move(convolutionWithMask(mainImage, masksGenerator(R5, L5)));
	lawsMass["mE5S5"] = std::move(convolutionWithMask(mainImage, masksGenerator(E5, S5)));
	lawsMass["mS5E5"] = std::move(convolutionWithMask(mainImage, masksGenerator(S5, E5)));
	lawsMass["mE5R5"] = std::move(convolutionWithMask(mainImage, masksGenerator(E5, R5)));
	lawsMass["mR5E5"] = std::move(convolutionWithMask(mainImage, masksGenerator(R5, E5)));
	lawsMass["mS5R5"] = std::move(convolutionWithMask(mainImage, masksGenerator(S5, R5)));
	lawsMass["mR5S5"] = std::move(convolutionWithMask(mainImage, masksGenerator(R5, S5)));
	lawsMass["mE5E5"] = std::move(convolutionWithMask(mainImage, masksGenerator(E5, E5)));
	lawsMass["mS5S5"] = std::move(convolutionWithMask(mainImage, masksGenerator(S5, S5)));
	lawsMass["mR5R5"] = std::move(convolutionWithMask(mainImage, masksGenerator(R5, R5)));
	
	cv::Mat filterImageMass[15];

	laws[1] = meanMask(lawsMass.at("mL5E5"), lawsMass.at("mE5L5"));
	laws[2] = meanMask(lawsMass.at("mL5R5"), lawsMass.at("mR5L5"));
	laws[3] = meanMask(lawsMass.at("mE5S5"), lawsMass.at("mS5E5"));
	laws[4] = lawsMass.at("mS5S5");
	laws[5] = lawsMass.at("mR5R5");
	laws[6] = meanMask(lawsMass.at("mL5S5"), lawsMass.at("mL5S5"));
	laws[7] = lawsMass.at("mE5E5");
	laws[8] = meanMask(lawsMass.at("mE5R5"), lawsMass.at("mR5E5"));
	laws[9] = meanMask(lawsMass.at("mS5R5"), lawsMass.at("mS5R5"));
	showImage(&laws[3]);
	
	calcMat();
	for (short i = 1; i < 10; ++i){
		calcDisp(laws[i]);
	}
	filterImageWithMask(mainImage);
	return main;
}

cv::Mat LawsEnergyMap::normedImage(cv::Mat& mainImage) {
	cv::Mat retImage = cv::Mat::zeros(mainImage.size(), CV_8UC1);
	mainImage.convertTo(mainImage, CV_8UC1);
	size_t localRowNumber = mainImage.rows - 15;
	size_t localColNumber = mainImage.cols - 15;

	for (size_t i = 0; i < localRowNumber; i++) {
		for (size_t j = 0; j < localColNumber; j++) {
			unsigned localMean = 0;
			for (size_t k = 0; k < 15; ++k){
				localMean += meanValueFinder(mainImage, j, i+k);
			}
			localMean /= 255;
			retImage.at<uchar>(i, j) = mainImage.at<uchar>(i + 7, j + 7) - localMean;
		}
	}
	return retImage; 
}

unsigned LawsEnergyMap::meanValueFinder(const cv::Mat& image, size_t localRow, size_t localCol) {
	int meanReturn = 0;
	for (size_t i = 0; i < 15; i++){
		meanReturn += image.at<uchar>(cv::Point(localRow+i, localCol));
	}

	return meanReturn;
}

cv::Mat LawsEnergyMap::masksGenerator(const cv::Mat& mask1, const cv::Mat& mask2) {
	cv::Mat temp = cv::Mat::zeros(cv::Size(5,5), CV_32F);

	temp = mask1*mask2.t();
	/*
	for (int i = 0; i < 5; ++i){
		for (int j = 0; j < 5; ++j){
			std::cout << temp.at<float>(i, j) << " ";
		}
		std::cout << std::endl;
	}*/
	return temp;
}

cv::Mat LawsEnergyMap::convolutionWithMask(const cv::Mat& mainImage, const cv::Mat& mask) {
	cv::Mat dst = cv::Mat::zeros(cv::Size(5, 5), CV_32F);
	cv::filter2D(mainImage, dst, -1, mask);
	dst.convertTo(dst, CV_32F);
	return dst;
}

cv::Mat LawsEnergyMap::meanMask(const cv::Mat& firstMask, const cv::Mat& secondMask) {
	cv::Mat temp(firstMask.size(), CV_32F);
	for (size_t i = 0; i < temp.size().height; ++i){
		for (size_t j = 0; j < temp.size().width; ++j){
			temp.at<float>(i, j) = (firstMask.at<float>(i, j) + secondMask.at<float>(i, j)) / 2;
		}
	}
	return temp;
}

void LawsEnergyMap::calcMat() {
	for (short i = 1; i < 10; ++i){
		meanVal[i] = cv::mean(laws[i])[0];
		std::cout<<"mean: " << meanVal[i] << " " << i <<std::endl;
	}
}

void LawsEnergyMap::calcDisp(const cv::Mat& laws) {
	double sum = 0;
	int h = laws.size().height;
	int w = laws.size().width;
	for (int i = 0; i<h; i++){
		for (int j = 0; j<w; j++){
			sum += (laws.at<float>(i, j) - meanVal[i]);
			//std::cout << sum << " ";
		}
	}
	sum /= (h*w);
	std::cout << "disp: " << sum << std::endl;
}

void LawsEnergyMap::filterImageWithMask(cv::Mat& in) {
	cv::Mat mE5S5 = convolutionWithMask(in, masksGenerator(S5,L5));
	cv::Mat mS5E5 = convolutionWithMask(in, masksGenerator(L5,S5));
	cv::Mat meanImage = meanMask(mE5S5, mS5E5);

	int h = meanImage.size().height;
	int w = meanImage.size().width;
	int ww = 15;
	cv::Mat out(h - ww, w - ww, CV_32F);
	for (int y = 0; y<h - ww; y++){
		for (int x = 0; x<w - ww; x++){
			const float& val = meanImage.at<float>(y + ww / 2, x + ww / 2);

			cv::Mat region = cv::Mat(meanImage, cv::Rect(x, y, ww, ww));
			float meanRect = mean(region)[0];
			float & v = out.at<float>(y, x);
			v = 0;
			if (meanRect > 0 && meanRect < 50){
				v = 0;
			}
			else if (meanRect >= 50 && meanRect < 100){
				v = 150;
			}
			else{
				v = 255;
			}
		}
	}

	//�����������
	out.convertTo(out, CV_8U);
	medianBlur(out, out, 55);
	showImage(&out);
}

