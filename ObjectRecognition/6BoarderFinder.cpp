#include "BoarderFinder.h"

#define CONTOURS_NUMBER 0

BoarderFinder::BoarderFinder() {

}

cv::Mat BoarderFinder::imageProcessing(cv::Mat& image) {
	IplImage* contouredImage = findContours1(image);
	rotateImage(contouredImage, 90);
	graphicDrawer(CONTOURS_NUMBER);
	
	return contouredImage;
}

IplImage* BoarderFinder::findContours1(cv::Mat& image) {
	IplImage* image1 = new IplImage(image);
	IplImage* bin = cvCreateImage(cvGetSize(image1), IPL_DEPTH_8U, 1);
	IplImage* dst = cvCloneImage(image1);

	cvInRangeS(image1, cvScalar(40), cvScalar(150), bin);
	//showImage(&Mat(image1, true));
	findFreeman(image1, CONTOURS_NUMBER);
	
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contours = 0;
	int contoursCont = cvFindContours(bin, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
	std::cout << "number of contours: " << contoursCont << std::endl;
	for (CvSeq* seq0 = contours; seq0 != 0; seq0 = seq0->h_next) {
		cvDrawContours(dst, seq0, CV_RGB(355, 200, 0), CV_RGB(0, 0, 200), 0, 1, 8); // ������ ������
	}

	return dst;
}

void BoarderFinder::findFreeman(IplImage* image, const int contourNumber) {
	vector<Vec4i> hierarchy;
	findContours(Mat(image, true), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(0, 0));

	int difX, difY;
	for (int i = 0; i < contours[contourNumber].size(); i++){
		if (i == (contours[contourNumber].size() - 1)){
			difX = contours[contourNumber][0].x - contours[contourNumber][i].x;
			difY = contours[contourNumber][0].y - contours[contourNumber][i].y;
		}
		else {
			difX = contours[contourNumber][i + 1].x - contours[contourNumber][i].x;
			difY = contours[contourNumber][i + 1].y - contours[contourNumber][i].y;
		}
		if ((difX == 0) && (difY == 1))
			freeman.push_back(6);
		if ((difX == 0) && (difY == -1))
			freeman.push_back(2);
		if ((difX == 1) && (difY == 0))
			freeman.push_back(0);
		if ((difX == -1) && (difY == 0))
			freeman.push_back(4);
		if ((difX == 1) && (difY == 1))
			freeman.push_back(7);
		if ((difX == -1) && (difY == -1))
			freeman.push_back(3);
		if ((difX == 1) && (difY == -1))
			freeman.push_back(1);
		if ((difX == -1) && (difY == 1))
			freeman.push_back(5);
	}
	for (auto s : freeman){
		std::cout << s << " ";
	}
	std::cout << std::endl;
	for (auto s : freeman){
		std::cout << s << " ";
	}
}

void BoarderFinder::firstDifferenceFinder() {
	int dif;
	for (int i = 0; i < freeman.size(); i++){
		if (i == (freeman.size() - 1))
			dif = freeman[0] - freeman[i];
		else
			dif = freeman[i + 1] - freeman[i];
		if (dif < 0)
			dif = 8 + dif;
		firstDifference.push_back(dif);
	}
}

void BoarderFinder::graphicDrawer(int countourNumber) {
	Moments mu = moments(contours[countourNumber], false);
	Point2f massCentre = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);
	std::cout << "center mass: " << massCentre.x << " " << massCentre.y <<std::endl;
	Mat xCoords(contours[countourNumber].size(), 1, CV_32FC1);
	Mat yCoords(contours[countourNumber].size(), 1, CV_32FC1);
	for (int i = 0; i < contours[countourNumber].size(); i++) {
		xCoords.at<float>(i, 0) = contours[countourNumber][i].x - massCentre.x;
		yCoords.at<float>(i, 0) = contours[countourNumber][i].y - massCentre.y;
	}
	Mat magnitude, anglePolar;
	cartToPolar(xCoords, yCoords, magnitude, anglePolar, 1);
	float max = 0;
	for (int i = 0; i < magnitude.rows; i++){
		if (magnitude.at<float>(i, 0) > max){
			max = magnitude.at<float>(i, 0);
		}
	}
	for (int i = 0; i < magnitude.rows; i++){
		magnitude.at<float>(i, 0) = magnitude.at<float>(i, 0) * 255.0 / max;
	}
	Mat result2 = Mat::zeros(255, anglePolar.rows+1, CV_8UC1);
	int xTemp, yTemp;

	for (int i = 0; i < anglePolar.rows; i++) {
		xTemp = 255 - magnitude.at<float>(i, 0);
		yTemp = anglePolar.rows - anglePolar.at<float>(i, 0) * (anglePolar.rows / 360.0);
		result2.at<uchar>(xTemp, yTemp) = 255;
	}
	showImage(&result2);
}

void BoarderFinder::rotateImage(Mat sourceImage, int angle) {
	int len = std::max(sourceImage.cols, sourceImage.rows);
	cv::Point2f pt(len / 2., len / 2.);
	cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
	cv::Mat dstImage = cv::Mat::zeros(sourceImage.size(), sourceImage.type());
	cv::warpAffine(sourceImage, dstImage, r, cv::Size(len, len));
	showImage(&dstImage);
	//return dstImage;
}

void BoarderFinder::mainTestMethod(Mat image) {

	for (int i = 0; i < image.rows; i++){
		for (int j = 0; j < image.cols; j++){
			if (image.at<uchar>(i, j) > 0){
				image.at<uchar>(i, j) = 255;
			}
		}
	}
	int scaleFactor = 1;
	Mat imageScaled = Mat::zeros(image.rows / scaleFactor, image.cols / scaleFactor, CV_8UC1);

	for (int i = 0; i < image.rows; i++){
		for (int j = 0; j < image.cols; j++){
			if (image.at<uchar>(i, j) > 0){
				imageScaled.at<uchar>(i / scaleFactor, j / scaleFactor) = 255;
			}
		}
	}
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(imageScaled, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(0, 0));

	vector <int> freeman;

	int difX, difY;
	for (int i = 0; i < contours[1].size(); i++)
	{
		if (i == (contours[1].size() - 1))
		{
			difX = contours[1][0].x - contours[1][i].x;
			difY = contours[1][0].y - contours[1][i].y;
		}
		else
		{
			difX = contours[1][i + 1].x - contours[1][i].x;
			difY = contours[1][i + 1].y - contours[1][i].y;
		}
		if ((difX == 0) && (difY == 1))
			freeman.push_back(6);
		if ((difX == 0) && (difY == -1))
			freeman.push_back(2);
		if ((difX == 1) && (difY == 0))
			freeman.push_back(0);
		if ((difX == -1) && (difY == 0))
			freeman.push_back(4);
		if ((difX == 1) && (difY == 1))
			freeman.push_back(7);
		if ((difX == -1) && (difY == -1))
			freeman.push_back(3);
		if ((difX == 1) && (difY == -1))
			freeman.push_back(1);
		if ((difX == -1) && (difY == 1))
			freeman.push_back(5);
	}

	int sdvig = 0;
	vector <int> invariantFreeman = freeman;
	int temp;
	for (int i = 0; i < freeman.size() - 1; i++)
	{
		temp = freeman[0];
		for (int j = 0; j < freeman.size() - 1; j++)
		{
			freeman[j] = freeman[j + 1];
		}
		freeman[freeman.size() - 1] = temp;

		for (int j = 0; j < freeman.size(); j++)
		{
			if (freeman[j] != invariantFreeman[j])
			{
				if (freeman[j] < invariantFreeman[j])
				{
					invariantFreeman = freeman;
					sdvig = i + 1;
				}
				break;
			}
		}
	}

	vector <int> firstDifference;
	int dif;
	for (int i = 0; i < invariantFreeman.size(); i++)
	{
		if (i == (invariantFreeman.size() - 1))
			dif = invariantFreeman[0] - invariantFreeman[i];
		else
			dif = invariantFreeman[i + 1] - invariantFreeman[i];
		if (dif < 0)
			dif = 8 + dif;
		firstDifference.push_back(dif);
	}

	vector <int> invariantFirstDifference = firstDifference;
	for (int i = 0; i < firstDifference.size() - 1; i++)
	{
		temp = firstDifference[0];
		for (int j = 0; j < firstDifference.size() - 1; j++)
		{
			firstDifference[j] = firstDifference[j + 1];
		}
		firstDifference[firstDifference.size() - 1] = temp;

		for (int j = 0; j < firstDifference.size(); j++)
		{
			if (firstDifference[j] != invariantFirstDifference[j])
			{
				if (firstDifference[j] < invariantFirstDifference[j])
				{
					invariantFirstDifference = firstDifference;
					sdvig += i + 1;
				}
				break;
			}
		}
	}

	sdvig = sdvig % (invariantFirstDifference.size());

	vector <pair<bool, int>> corners;			//0 - ������� �������, 1 - ��������, ����� ����� � ������ contours[1]
	vector <int> outCorners;					//�������� �������� �������
	pair <bool, int> tempCorner;

	for (int i = 0; i < invariantFirstDifference.size(); i++)
	{
		if ((invariantFirstDifference[i] < 4) && (invariantFirstDifference[i] > 0))
		{
			tempCorner.first = 0;
			tempCorner.second = (i + sdvig) % invariantFirstDifference.size();
			corners.push_back(tempCorner);
			//inCorners.push_back(tempCorner.second);
		}
		if (invariantFirstDifference[i] > 4)
		{
			tempCorner.first = 1;
			tempCorner.second = (i + sdvig) % invariantFirstDifference.size();
			corners.push_back(tempCorner);
			outCorners.push_back(tempCorner.second);
		}
	}

	double angle1, angle2, angle;					//���������� �������� ������, �������� �������� ������� ��������
	for (int i = 0; i < outCorners.size(); i++)
	{
		if (i == outCorners.size() - 2)
		{
			angle1 = atan2(double(contours[1][outCorners[i + 1]].y - contours[1][outCorners[i]].y),
				double(contours[1][outCorners[i + 1]].x - contours[1][outCorners[i]].x)) * 180.0 / CV_PI;
			angle2 = atan2(double(contours[1][outCorners[0]].y - contours[1][outCorners[i + 1]].y),
				double(contours[1][outCorners[0]].x - contours[1][outCorners[i + 1]].x)) * 180.0 / CV_PI;
		}
		if (i == outCorners.size() - 1)
		{
			angle1 = atan2(double(contours[1][outCorners[0]].y - contours[1][outCorners[i]].y),
				double(contours[1][outCorners[0]].x - contours[1][outCorners[i]].x)) * 180.0 / CV_PI;
			angle2 = atan2(double(contours[1][outCorners[1]].y - contours[1][outCorners[0]].y),
				double(contours[1][outCorners[1]].x - contours[1][outCorners[0]].x)) * 180.0 / CV_PI;
		}
		if (i < outCorners.size() - 2)
		{
			angle1 = atan2(double(contours[1][outCorners[i + 1]].y - contours[1][outCorners[i]].y),
				double(contours[1][outCorners[i + 1]].x - contours[1][outCorners[i]].x)) * 180.0 / CV_PI;
			angle2 = atan2(double(contours[1][outCorners[i + 2]].y - contours[1][outCorners[i + 1]].y),
				double(contours[1][outCorners[i + 2]].x - contours[1][outCorners[i + 1]].x)) * 180.0 / CV_PI;
		}
		angle = 180 - angle1 + angle2;

		if (angle >= 180)
		{
			temp = (i + 1) % outCorners.size();
			for (int j = 0; j < corners.size(); j++)
				if (corners[j].second == outCorners[temp])
					corners.erase(corners.begin() + j);
			outCorners.erase(outCorners.begin() + temp);
			i--;
		}
	}

	int next;
	for (int i = 0; i < corners.size(); i++)				//�������� ������� ����� � ���������
	{
		if (i == corners.size() - 1)
			next = 0;
		else
			next = i + 1;
		if ((corners[i].first != corners[next].first) &&
			((abs(contours[1][corners[i].second].x - contours[1][corners[next].second].x) <= 1) ||
			(abs(contours[1][corners[i].second].y - contours[1][corners[next].second].y) <= 1)))
		{
			if (corners[i].first == 0)
				corners.erase(corners.begin() + i);
			else
				corners.erase(corners.begin() + next);
			i--;
		}
	}
	/*
	bool go = 1;
	int noInterest = 0;
	while (go){						//���������� �������������� ����� �������� � ������� ���������� �������, ���� �������� ������ ������� - ���������
		go = 0;
		noInterest = 0;

		for (int i = 0; i < corners.size(); i++)
		{
			if ((i == corners.size() - 2) && (corners[i + 1].first == 1))
			{
				angle1 = atan2(double(contours[1][corners[i + 1].second].y - contours[1][corners[i].second].y),
					double(contours[1][corners[i + 1].second].x - contours[1][corners[i].second].x)) * 180.0 / CV_PI;
				angle2 = atan2(double(contours[1][corners[0].second].y - contours[1][corners[i + 1].second].y),
					double(contours[1][corners[0].second].x - contours[1][corners[i + 1].second].x)) * 180.0 / CV_PI;
			}
			else
				noInterest++;
			if ((i == outCorners.size() - 1) && (corners[0].first == 1))
			{
				angle1 = atan2(double(contours[1][corners[0].second].y - contours[1][corners[i].second].y),
					double(contours[1][corners[0].second].x - contours[1][corners[i].second].x)) * 180.0 / CV_PI;
				angle2 = atan2(double(contours[1][corners[1].second].y - contours[1][corners[0].second].y),
					double(contours[1][corners[1].second].x - contours[1][corners[0].second].x)) * 180.0 / CV_PI;
			}
			else
				noInterest++;
			if ((i < outCorners.size() - 2) && (corners[i + 1].first == 1))
			{
				angle1 = atan2(double(contours[1][corners[i + 1].second].y - contours[1][corners[i].second].y),
					double(contours[1][corners[i + 1].second].x - contours[1][corners[i].second].x)) * 180.0 / CV_PI;
				angle2 = atan2(double(contours[1][corners[i + 2].second].y - contours[1][corners[i + 1].second].y),
					double(contours[1][corners[i + 2].second].x - contours[1][corners[i + 1].second].x)) * 180.0 / CV_PI;
			}
			else
				noInterest++;

			if (noInterest < 3)
			{
				angle = 180 - angle1 + angle2;

				if (angle >= 180)
				{
					corners.erase(corners.begin() + i);
					i--;
					go = 1;
				}
			}
		}
	}
	*/
	Mat result = Mat::zeros(image.rows, image.cols, CV_8UC1);
	for (int i = 0; i < corners.size(); i++)
	{
		contours[1][corners[i].second].x = contours[1][corners[i].second].x * scaleFactor;
		contours[1][corners[i].second].y = contours[1][corners[i].second].y * scaleFactor;
	}

	for (int i = 0; i < corners.size() - 1; i++)
	{
		line(result, contours[1][corners[i].second], contours[1][corners[i + 1].second], 255, 1, 8, 0);
		line(image, contours[1][corners[i].second], contours[1][corners[i + 1].second], 128, 1, 8, 0);
	}
	line(result, contours[1][corners[corners.size() - 1].second], contours[1][corners[0].second], 255, 1, 8, 0);
	line(image, contours[1][corners[corners.size() - 1].second], contours[1][corners[0].second], 128, 1, 8, 0);


	/*imwrite("result30.bmp", result);
	imwrite("result31.bmp", image);*/
	/*
	ofstream outputFreeman;
	outputFreeman.open("freeman.txt");
	ofstream outputDifference;
	outputDifference.open("firstDifference.txt");
	for (int i = 0; i < invariantFreeman.size(); i++)
	{
		outputFreeman << invariantFreeman[i];
		outputDifference << invariantFirstDifference[i];
	}
	outputFreeman.close();
	outputDifference.close();
	*/

	Moments mu = moments(contours[1], false);
	Point2f massCentre = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);
	Mat xCoords(contours[1].size(), 1, CV_32FC1);
	Mat yCoords(contours[1].size(), 1, CV_32FC1);
	for (int i = 0; i < contours[1].size(); i++)
	{
		xCoords.at<float>(i, 0) = contours[1][i].x - massCentre.x;
		yCoords.at<float>(i, 0) = contours[1][i].y - massCentre.y;
	}
	Mat magnitude, anglePolar;
	cartToPolar(xCoords, yCoords, magnitude, anglePolar, 1);
	float max = 0;
	for (int i = 0; i < magnitude.rows; i++)
	{
		if (magnitude.at<float>(i, 0) > max)
		{
			max = magnitude.at<float>(i, 0);
		}
	}
	for (int i = 0; i < magnitude.rows; i++)
	{
		magnitude.at<float>(i, 0) = magnitude.at<float>(i, 0) * 255.0 / max;
	}
	Mat result2 = Mat::zeros(256, anglePolar.rows+1, CV_8UC1);
	int xTemp, yTemp;
	
					 
	for (int i = 0; i < anglePolar.rows; i++)
	{
		xTemp = 255 - magnitude.at<float>(i, 0);
		yTemp = anglePolar.rows - anglePolar.at<float>(i, 0) * (anglePolar.rows / 360.0);
		result2.at<uchar>(xTemp, yTemp) = 255;
	}
	showImage(&result2);
}