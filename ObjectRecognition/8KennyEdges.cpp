#include "KennyEdges.h"

cv::Mat KennyEdges::imageProcessing(cv::Mat& mainImage){
	cv::Mat detectedEdges, outputBin;
	double lowThereshold = 50, ratio = 50, kernelSize = 5;
	cv::blur(mainImage, detectedEdges, cv::Size(3, 3));
	cv::Canny(detectedEdges, outputBin, lowThereshold, lowThereshold*ratio, kernelSize);

	//showImage(&outputBin);
	return outputBin;
}
