#ifndef _ATH
#define _ATH
#include "StrategyMethod.h"

using namespace std;
using namespace cv;

class AffineTransform : public StrategyMethod {
public:
	cv::Mat imageProcessing(cv::Mat&);
	pair<double, double> centerMassCalc(cv::Mat&);

protected:
	double xPlus(double x, double y, pair<double, double>);
	double yPlus(double x, double y, pair<double, double>);
	
	void showImage(const cv::Mat*);

private:
	//double T = 0;
	double mu = 0;
	double th = 0;

};

#endif //_ATH
