#ifndef _BF
#define _BF

#include "StrategyMethod.h"

using namespace cv;
using namespace std;

class BoarderFinder : public StrategyMethod {
public:
	BoarderFinder();
	IplImage* findContours1(cv::Mat&);
	void mainTestMethod(Mat image);
	cv::Mat imageProcessing(cv::Mat&);

private:
	void findFreeman(IplImage*, const int);
	void firstDifferenceFinder();
	void graphicDrawer(int);
	void rotateImage(Mat, int);

	vector<int> firstDifference;
	vector<int> freeman;
	vector<vector<Point> > contours;
};

#endif //_BF