#include "Co-occurenceMatrix.h"

CoocurrenceMatrix::CoocurrenceMatrix() {
	displacementVector[0] = std::make_pair(0, 1);
	displacementVector[1] = std::make_pair(1, 0);
	displacementVector[2] = std::make_pair(1, 1);
	displacementVector[3] = std::make_pair(0, -1);
	displacementVector[4] = std::make_pair(-1, 0);
	displacementVector[5] = std::make_pair(-1, -1);
	displacementVector[6] = std::make_pair(-1, 1);
	displacementVector[7] = std::make_pair(1, -1);
	//scatterMatrix = cv::Mat::zeros(cv::Size(255, 255), CV_32F);
}

CoocurrenceMatrix::~CoocurrenceMatrix() {
	scatterMatrix.release();
}

cv::Mat CoocurrenceMatrix::imageProcessing(cv::Mat& image) {
#define DP 1
	this->normedScutterMatrix = createScatterMatrix(image, displacementVector[DP], false);
	std::cout << "covariation: " << covariation(matExpected()) << std::endl;
	std::cout << "inertiaMoment: " << inertiaMoment() << std::endl;
	std::cout << "meanAbsolutDifference: " << meanAbsolutDifference() << std::endl;
	std::cout << "energy: " << energy() << std::endl;
	std::cout << "entropy: " << entropy() << std::endl;
	std::cout << "backwardDifference: " << backwardDifference() << std::endl;
	std::cout << "uniformity: " << uniformity() << std::endl;
	std::cout << "correlationCoefficient: " << correlationCoefficient(matExpected()) << std::endl;
	hiCritary(image);
	filterImageWithMask(image);
	return this->normedScutterMatrix;
}

cv::Mat CoocurrenceMatrix::createScatterMatrix(
	const cv::Mat& image, 
	const std::pair<short, short>& displacementVector,
	bool normScutterMatrix) {

	cv::Mat tempScatterMatrix = cv::Mat::zeros(cv::Size(256, 256), CV_32F);
	cv::Mat unnormedScatterMatrix = cv::Mat::zeros(cv::Size(256, 256), CV_32F);
	for (short i = 1; i < 255-1; ++i) {
		for (short j = 1; j < 255-1; ++j) {
			uchar a = image.at<uchar>(i, j);
			uchar b = image.at<uchar>(i + displacementVector.first, j + displacementVector.second);
			if (a < 0 || b < 0 || a>255 || b > 255){
				printf("ERROR");
			}
			tempScatterMatrix.at<float>(a, b)++;
		}
	}
	float sumValue = cv::sum(tempScatterMatrix)[0];
	tempScatterMatrix.copyTo(unnormedScatterMatrix);
	for (short i = 0; i < 255; ++i){
		for (short j = 0; j < 255; ++j){
			tempScatterMatrix.at<float>(i, j) /= sumValue;
		}
	}
	this->scatterMatrix = tempScatterMatrix;
	
	if (normScutterMatrix){
		return tempScatterMatrix;
	}
	else{
		return unnormedScatterMatrix;
	}
}

std::pair<float, float> CoocurrenceMatrix::matExpected() {
	std::pair<float, float> matExpected;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			//assert(scatterMatrix.at<float>(a, b) <= 1);
			matExpected.first += a*scatterMatrix.at<float>(a, b);
			matExpected.second += b*scatterMatrix.at<float>(a, b);
		}
	}
	return matExpected;
}

float CoocurrenceMatrix::covariation(std::pair<float,float> matExpected) {
	float covariation = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			covariation += (a - matExpected.first)*(b - matExpected.second)*scatterMatrix.at<float>(a, b);
		}
	}
	return covariation;
}

float CoocurrenceMatrix::inertiaMoment() {
	float inertiaMoment = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			inertiaMoment += std::pow((a - b), 2)*scatterMatrix.at<float>(a, b);
		}
	}
	return inertiaMoment;
}

float CoocurrenceMatrix::meanAbsolutDifference() {
	float meanDiff = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			meanDiff += std::abs(a - b) * scatterMatrix.at<float>(a, b);
		}
	}
	return meanDiff;
}

float CoocurrenceMatrix::energy() {
	float energy = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			energy += std::pow(scatterMatrix.at<float>(a, b), 2);
		}
	}
	return energy;
}

float CoocurrenceMatrix::entropy() {
	float entropy = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			if (scatterMatrix.at<float>(a, b) != 0){
				entropy += scatterMatrix.at<float>(a, b)*log2(scatterMatrix.at<float>(a, b));
			}
		}
	}
	return -entropy;
}

float CoocurrenceMatrix::backwardDifference() {
	float backwardDiff = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			backwardDiff += scatterMatrix.at<float>(a, b) / (1 + (a - b)*(a - b));
		}
	}
	return backwardDiff;
}

float CoocurrenceMatrix::uniformity() {
	float uniform = 0;
	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			uniform += scatterMatrix.at<float>(a, b) / (1 + abs(a - b));
		}
	}
	return uniform;
}

float CoocurrenceMatrix::correlationCoefficient(std::pair<float, float> matExpected) {
	float coefficient = 0;
	std::pair<float, float> standartDeviation;
	for (int a = 1; a < 255; ++a){
		for (int b = 1; b < 255; ++b){
			coefficient = a*b*scatterMatrix.at<float>(a, b) - matExpected.first*matExpected.second;
			standartDeviation.first += a*a*scatterMatrix.at<float>(a, b) - std::pow(matExpected.first, 2);
			standartDeviation.second += b*b*scatterMatrix.at<float>(a, b) - std::pow(matExpected.second, 2);
		}
	}
	return coefficient/(standartDeviation.first*standartDeviation.second);
}

void CoocurrenceMatrix::hiCritary(cv::Mat& image) {
	double hi;
	for (short i = 0; i < 8; ++i){
	cv::Mat temp = createScatterMatrix(image, displacementVector[i], true);
	hi=0;

	for (int a = 0; a < 255; ++a){
		for (int b = 0; b < 255; ++b){
			float pa = 0, pb = 0;
			for (int bp = 0; bp < 255; ++bp){
				pb += temp.at<float>(bp, b)+0.0001;
			}
			for (int ap = 0; ap < 255; ++ap){
				pa += temp.at<float>(a, ap)+0.0001;
			}
			//std::cout << pa;
			hi += (std::pow(temp.at<float>(a, b), 2) / (pa*pb));
		}
		}
	std::cout << hi-1 << std::endl;
	}
}

void CoocurrenceMatrix::filterImageWithMask(cv::Mat& meanImage) {
	int h = meanImage.size().height;
	int w = meanImage.size().width;
	cv::Mat temp;
	meanImage.convertTo(temp, CV_32F);
	int ww = 15;
	cv::Mat out(h - ww, w - ww, CV_32F);
	for (int y = 0; y<h - ww; y++){
		for (int x = 0; x<w - ww; x++){
			const float& val = temp.at<float>(y + ww / 2, x + ww / 2);
			 
			cv::Mat region = cv::Mat(temp, cv::Rect(x, y, ww, ww));
			float meanRect = mean(region)[0];
			float & v = out.at<float>(y, x);
			v = 0;
			if (meanRect > 0 && meanRect < 60){
				v = 0;
			}
			else if (meanRect >= 60 && meanRect < 150){
				v = 150;
			}
			else{
				v = 255;
			}
		}
	}

	//�����������
	out.convertTo(out, CV_8U);
	medianBlur(out, out, 55);
	showImage(&out);
}