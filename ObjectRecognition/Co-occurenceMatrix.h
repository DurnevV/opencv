#ifndef _CoM
#define _CoM

#include "StrategyMethod.h"

class CoocurrenceMatrix : public StrategyMethod {
public:
	CoocurrenceMatrix();
	~CoocurrenceMatrix();

	cv::Mat imageProcessing(cv::Mat&);
	void filterImageWithMask(cv::Mat&);

protected:
	cv::Mat createScatterMatrix(const cv::Mat& ,const std::pair<short, short>&, bool);
	
	//---�������� �������
	std::pair<float, float> matExpected();
	float covariation(std::pair<float, float>);
	float inertiaMoment();
	float meanAbsolutDifference();
	float energy();
	float entropy();
	float backwardDifference();
	float uniformity();
	float correlationCoefficient(std::pair<float, float>);
	void hiCritary(cv::Mat&);

private:
	cv::Mat scatterMatrix, normedScutterMatrix;
	std::pair<short, short> displacementVector[8];

};

#endif // _CoM