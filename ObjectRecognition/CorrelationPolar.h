#ifndef _CPH
#define _CPH
#include "StrategyMethod.h"

class CorrelationPolar : public StrategyMethod {
public:
	~CorrelationPolar(){};
	cv::Mat imageProcessing(cv::Mat&);
	
private:
	cv::Mat convertImageIntoPolar(cv::Mat&);
	cv::Mat outlineTheCircle(cv::Mat&);
	cv::Mat rotateImage(cv::Mat&, cv::Mat& , double );

};

#endif