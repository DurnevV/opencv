#ifndef _ECH
#define _ECH

#include "StrategyMethod.h"

class EtalonCorrelationSearch : public StrategyMethod {
public:
	~EtalonCorrelationSearch(){};
	EtalonCorrelationSearch(std::string, short, float);
	cv::Mat imageProcessing(cv::Mat&);

private:
	cv::Mat normalizeImage(cv::Mat&);
	void rotateImage(cv::Mat&, cv::Mat&, double);

	cv::Mat mainImage;
	short actionType;
	float factor;
};

#endif //_ECH