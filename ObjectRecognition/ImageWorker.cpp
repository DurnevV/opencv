#include "ImageWorker.h"

ImageWorker::ImageWorker(StrategyMethod* WA, int imageNumber) : workingAlg(WA), image(imageNumber) {
	mainImage = cv::imread("squareMain.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	searchImage = cv::imread("search.bmp", CV_LOAD_IMAGE_GRAYSCALE);
} 

ImageWorker::ImageWorker(StrategyMethod* WA, char* imageName) : workingAlg(WA), image(0) {
	defaultImage = cv::imread(imageName, CV_LOAD_IMAGE_GRAYSCALE);
	//showImage(&workingAlg->imageProcessing());
}

ImageWorker::~ImageWorker() {
	delete workingAlg;
	mainImage.release();
	searchImage.release();
}

void ImageWorker::workWithTheImage() {
	switch (image){
	case 1:
		showImage(&workingAlg->imageProcessing(mainImage));
		break;
	case 2:
		showImage(&workingAlg->imageProcessing(searchImage));
		break;
	case 0:
		showImage(&workingAlg->imageProcessing(defaultImage));
		break;
	default:
		std::cout << "error: no such image";
		break;
	}
}

void ImageWorker::showImage(const cv::Mat* imageToShow) {
	cv::namedWindow("image", CV_WINDOW_AUTOSIZE);
	imshow("image", *imageToShow);
	cv::waitKey();
}

