#ifndef _IHANDLER
#define _IHANDLER

#include <opencv2\opencv.hpp>
#include "EtalonCorrelation.h"
#include "OtsuThresholding.h"
#include "KennyEdges.h"
#include "CorrelationPolar.h"
#include "AffineTransform.h"
#include "LawsEnergyMap.h"
#include "Co-occurenceMatrix.h"
#include "BoarderFinder.h"

class ImageWorker {
public:
	ImageWorker(StrategyMethod*, int);
	ImageWorker(StrategyMethod*, char*);
	~ImageWorker();
	void workWithTheImage();
	void showImage(const cv::Mat*);

private:
	int image;
	StrategyMethod* workingAlg;
	cv::Mat mainImage, searchImage, defaultImage;
};

#endif //_IHANDLER