#ifndef _KEH
#define _KEH

#include "StrategyMethod.h"

class KennyEdges : public StrategyMethod {
public:
	~KennyEdges(){}
	cv::Mat imageProcessing(cv::Mat&);
};

#endif //_KEH