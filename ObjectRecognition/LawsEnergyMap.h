#ifndef _LEM
#define _LEM

#include "StrategyMethod.h"
#include <vector>
#include <unordered_map>

class LawsEnergyMap : public StrategyMethod {
public:
	LawsEnergyMap();
	~LawsEnergyMap();

	cv::Mat imageProcessing(cv::Mat&);
	cv::Mat normedImage(cv::Mat&);

private:
	unsigned meanValueFinder(const cv::Mat&, size_t, size_t);
	cv::Mat masksGenerator(const cv::Mat&, const cv::Mat&);
	cv::Mat convolutionWithMask(const cv::Mat&, const cv::Mat&);
	cv::Mat meanMask(const cv::Mat&, const cv::Mat&);
	void calcDisp(const cv::Mat&);
	void calcMat();
	void filterImageWithMask(cv::Mat&);

	cv::Mat L5;
	cv::Mat E5;
	cv::Mat S5;
	cv::Mat R5;
	cv::Mat laws[10];
	float meanVal[10];
	float dispVal[10];
};

#endif //_LEM