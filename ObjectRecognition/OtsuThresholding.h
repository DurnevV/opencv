#ifndef _OTH
#define _OTH

#include "StrategyMethod.h"

class OtsuThresholding : public StrategyMethod {
public:
	OtsuThresholding(bool);
	~OtsuThresholding(){}
	cv::Mat imageProcessing(cv::Mat&);
	cv::Mat otsuMultiTheresholding(cv::Mat&);
	cv::Mat otsuBinThreshholding(cv::Mat&);

private:
	bool multiOtsu;
};

#endif //_OTH