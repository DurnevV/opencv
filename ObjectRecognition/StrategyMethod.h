#ifndef _ISW
#define _ISW

#include "opencv2\opencv.hpp"
#include <string>

class StrategyMethod {
public:
	virtual ~StrategyMethod(){};
	virtual cv::Mat imageProcessing(cv::Mat&) = 0;

protected:
	void showImage(const cv::Mat* imageToShow){
		cv::namedWindow("image", CV_WINDOW_AUTOSIZE);
		imshow("image", *imageToShow);
		cv::waitKey();
	}
};

#endif //_ISW