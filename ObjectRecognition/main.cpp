#include "ImageWorker.h"

int main(void) {
	ImageWorker slave1(new EtalonCorrelationSearch("squareMain.bmp", 4, 0.12), 1);// done 
	ImageWorker slave2(new KennyEdges ,1);// done
	ImageWorker slave3(new CorrelationPolar, 1);// done
	ImageWorker slave4(new OtsuThresholding(true), 1);// done
	ImageWorker slave5(new LawsEnergyMap, "brickgrass.bmp"); // done
	ImageWorker slave6(new AffineTransform, 2); // done
	ImageWorker slave7(new CoocurrenceMatrix, "brickgrass.bmp");//done
	ImageWorker slave8(new BoarderFinder, "fig.png");

	slave8.workWithTheImage();
}
